# author: Sebastian Buschjaeger

#!/usr/bin/env python3

import numpy as np
import random 
from random import randint

# Path where generated data should be saved
outpath = "out.csv"

# Number of data points to be generated
NUMData = 1000000

# Number of true clusters 
NUMCluster = 3

# Dimension of each data point
DIM = 2

# Generate true cluster centers
mus = [np.random.normal(0,10,DIM) for i in range(NUMCluster)]

# Generte true cluster boundaries (= covariance matrix for gaussian)
covs = []
for i in range(NUMCluster):
	# Simple 'hack' to generate a symmetric positive definite matrix 
	cov = np.random.rand(DIM, DIM)
	cov = np.dot(cov, cov.T)
	cov = np.add(cov, DIM * np.identity(DIM))

	covs.append(cov)

print("cluster centers = ", mus)
print("covariance matrices = ", covs)

# Generate the actual data. Note: Every cluster has the same probability 
X = []
Y = []
for i in range(NUMData):
	clusterIndex = randint(0, NUMCluster - 1)
	Y.append(mus[clusterIndex])
	x = np.random.multivariate_normal(mus[clusterIndex], covs[clusterIndex])
	X.append(x)

# Prepare file
file = open(outpath, 'w')

# Print header
header = ""

# If you need the true cluster
# for i in range(DIM):
# 	header += "y_" + str(i) + ","

for i in range(DIM):
	header += "x_" + str(i) + ","

header = header[:-1]
file.write(header + "\n")

# Print the data
for x,y in zip(X,Y):
	s = ""
	
	# If you need the true cluster
	# for e in y:
	# 	s += str(e) + ","

	for e in x:
		s += str(e) + ","

	s = s[:-1]

	file.write(s + "\n")

file.close()
