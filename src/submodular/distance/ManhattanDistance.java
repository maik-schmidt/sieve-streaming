package submodular.distance;

import main.Data;

// L1-Norm: ||x - y||_1 = sum_i |x_i - y_i|
public class ManhattanDistance implements DistanceFunction {

    @Override
    public double distance(Data x, Data y) {
        double[] valuesX = x.getValues();
        double[] valuesY = y.getValues();

        double sum = 0.0;
        for (int i = 0; i < valuesX.length; i++) {
            sum += Math.abs(valuesX[i] - valuesY[i]);
        }
        return sum;
    }
}
