package submodular.distance;

import main.Data;

public interface DistanceFunction {

    // distance indicates dissimilarity between two vectors
    double distance(Data x, Data y);
}
