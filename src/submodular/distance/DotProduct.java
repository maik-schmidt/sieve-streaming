package submodular.distance;

import main.Data;

// x^T * y
public class DotProduct implements DistanceFunction {

    @Override
    public double distance(Data x, Data y) {
        double[] valuesX = x.getValues();
        double[] valuesY = y.getValues();

        double sum = 0.0;
        for (int i = 0; i < valuesX.length; i++) {
            sum += valuesX[i] * valuesY[i];
        }
        return sum;
    }
}
