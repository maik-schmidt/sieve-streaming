package submodular.distance;

import main.Data;

// L2-Norm: ||x - y||_2 = sqrt(sum_i (x_i - y_i)^2)
public class EuclideanDistance implements DistanceFunction {

    @Override
    public double distance(Data x, Data y) {
        double[] valuesX = x.getValues();
        double[] valuesY = y.getValues();

        double sum = 0.0;
        for (int i = 0; i < valuesX.length; i++) {
            double difference = valuesX[i] - valuesY[i];
            sum += difference * difference;
        }
        return Math.sqrt(sum);
    }
}
