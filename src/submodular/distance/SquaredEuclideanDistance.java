package submodular.distance;

import main.Data;

// ||x - y||^2 = sum_i (x_i - y_i)^2
public class SquaredEuclideanDistance implements DistanceFunction {

    @Override
    public double distance(Data x, Data y) {
        double[] valuesX = x.getValues();
        double[] valuesY = y.getValues();

        double sum = 0.0;
        for (int i = 0; i < valuesX.length; i++) {
            double difference = valuesX[i] - valuesY[i];
            sum += difference * difference;
        }
        return sum;
    }
}
