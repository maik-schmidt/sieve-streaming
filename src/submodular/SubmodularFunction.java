package submodular;

import main.Data;
import main.Sieve;

public interface SubmodularFunction {

    // calculate objective function of a single element
    double utility(Data element);

    // calculate objective function of a sieve's summary
    double utility(Sieve sieve);

    // calculate objective function of the union of a sieve's summary and an element
    double utility(Sieve sieve, Data element);

}
