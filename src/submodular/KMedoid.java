package submodular;

import main.Data;
import main.Sieve;
import submodular.distance.DistanceFunction;

public class KMedoid implements SubmodularFunction {

    private DistanceFunction distanceFunction;
    private Data[] reservoir;
    private double[] distance;

    // loss of auxiliary element (0,...,0)^T
    private double auxiliaryLoss;

    public KMedoid(DistanceFunction distanceFunction, Data[] reservoir) {
        this.distanceFunction = distanceFunction;
        this.reservoir = reservoir;
        distance = new double[reservoir.length];

        Data auxiliary = new Data(-1, new double[reservoir[0].getValues().length]);
        for (int i = 0; i < reservoir.length; i++) {
            auxiliaryLoss += distanceFunction.distance(reservoir[i], auxiliary);
        }
        auxiliaryLoss /= reservoir.length;
    }

    @Override
    public double utility(Data element) {
        double sum = 0.0;
        for (int i = 0; i < reservoir.length; i++) {
            distance[i] = distanceFunction.distance(reservoir[i], element);
            sum += distance[i];
        }
        sum /= reservoir.length;

        return auxiliaryLoss - sum;
    }

    @Override
    public double utility(Sieve sieve) {
        return sieve.getUtility();
    }

    @Override
    public double utility(Sieve sieve, Data element) {
        //Data[] summary = sieve.getSummary();
        double[] distance = sieve.getDistance();

        double sum = 0.0;
        for (int i = 0; i < reservoir.length; i++) {
            //double min = distanceFunction.distance(reservoir[i], element);
            //for (int j = 0; j < sieve.getSize(); j++) {
            //    double distance = distanceFunction.distance(reservoir[i], summary[j]);
            //    if (distance < min) {
            //        min = distance;
            //    }
            //}
            sum += Math.min(distance[i], this.distance[i]);
        }
        sum /= reservoir.length;

        return auxiliaryLoss - sum;
    }

    public Data[] getReservoir() {
        return reservoir;
    }

    public Data getReservoir(int i) {
        return reservoir[i];
    }

    public void setReservoir(Data[] reservoir) {
        this.reservoir = reservoir;
    }

    public double getAuxiliaryLoss() {
        return auxiliaryLoss;
    }

    public void setAuxiliaryLoss(double auxiliaryLoss) {
        this.auxiliaryLoss = auxiliaryLoss;
    }

    public double[] getDistance() {
        return distance;
    }
}
