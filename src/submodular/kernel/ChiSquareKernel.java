package submodular.kernel;

import main.Data;

// k(x, y) = 2 * sum_i(x_i * y_i / (x_i + y_i))
// positive-definite for relative frequencies (sum x_i = 1)
public class ChiSquareKernel implements KernelFunction {

    @Override
    public double kernel(Data x, Data y) {
        double[] valuesX = x.getValues();
        double[] valuesY = y.getValues();

        double sum = 0.0;
        for (int i = 0; i < valuesX.length; i++) {
            sum += valuesX[i] * valuesY[i] / (valuesX[i] + valuesY[i]);
        }
        return 2.0 * sum;
    }

    @Override
    public double kernel(Data x) {
        return 1.0;
    }

    @Override
    public boolean constantTrace() {
        return true;
    }
}
