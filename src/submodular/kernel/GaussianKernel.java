package submodular.kernel;

import main.Data;
import submodular.distance.SquaredEuclideanDistance;

// k(x, y) = e^(-||x - y||^2 / (2 * sigma^2)) = e^(-gamma * ||x - y||^2)
// positive-definite
public class GaussianKernel implements KernelFunction {

    // parameter of gaussian kernel
    private double gamma;

    private SquaredEuclideanDistance squaredEuclidean;

    // use default value 1.0 for sigma
    public GaussianKernel() {
        gamma = 1.0 / 2.0;
        squaredEuclidean = new SquaredEuclideanDistance();
    }

    public GaussianKernel(double sigma) {
        gamma = 1.0 / (2.0 * sigma * sigma);
        squaredEuclidean = new SquaredEuclideanDistance();
    }

    @Override
    public double kernel(Data x, Data y) {
        double squaredEuclid = squaredEuclidean.distance(x, y);
        return Math.exp(-gamma * squaredEuclid);
    }

    @Override
    public double kernel(Data x) {
        return 1.0;
    }

    @Override
    public boolean constantTrace() {
        return true;
    }
}
