package submodular.kernel;

import main.Data;
import submodular.distance.EuclideanDistance;

// k(x, y) = 0 if ||x - y|| >= sigma
// k(x, y) = 1 - 3/2 * ||x - y|| / sigma + 1/2 * (||x  - y|| / sigma)^3
// positive-definite in R^3
public class SphericalKernel implements KernelFunction {

    // parameter of circular kernel
    private double sigma;

    private EuclideanDistance euclidean;

    // use default value 1.0 for sigma
    public SphericalKernel() {
        sigma = 1.0;
        euclidean = new EuclideanDistance();
    }

    public SphericalKernel(double sigma) {
        this.sigma = sigma;
        euclidean = new EuclideanDistance();
    }

    @Override
    public double kernel(Data x, Data y) {
        double euclid = euclidean.distance(x, y);
        if (euclid >= sigma) {
            return 0.0;
        }
        euclid = euclid / sigma;
        return 1.0 - (3.0 * euclid + Math.pow(euclid, 3)) / 2.0;
    }

    @Override
    public double kernel(Data x) {
        return 1.0;
    }

    @Override
    public boolean constantTrace() {
        return true;
    }
}
