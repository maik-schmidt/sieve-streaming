package submodular.kernel;

import main.Data;

// k(x, y) = e^(-1 / sigma^2 * sum((x_i - y_i)^2 / (x + y))) = e^(-gamma * sum((x_i - y_i)^2 / (x + y)))
// positive-definite
public class ExponentialChiSquareKernel implements KernelFunction {

    // parameter of gaussian kernel
    private double gamma;

    // use default value 1.0 for sigma
    public ExponentialChiSquareKernel() {
        gamma = 1.0;
    }

    public ExponentialChiSquareKernel(double sigma) {
        gamma = 1.0 / (2.0 * sigma * sigma);
    }

    @Override
    public double kernel(Data x, Data y) {
        double[] valuesX = x.getValues();
        double[] valuesY = y.getValues();

        double sum = 0.0;
        for (int i = 0; i < valuesX.length; i++) {
            sum += Math.pow(valuesX[i] - valuesY[i], 2) / (valuesX[i] + valuesY[i]);
        }
        return Math.exp(-gamma * sum);
    }

    @Override
    public double kernel(Data x) {
        return 1.0;
    }

    @Override
    public boolean constantTrace() {
        return true;
    }
}
