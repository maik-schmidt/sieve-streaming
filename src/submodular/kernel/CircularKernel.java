package submodular.kernel;

import main.Data;
import submodular.distance.EuclideanDistance;

// k(x, y) = 0 if ||x - y|| >= sigma
// k(x, y) = 2 / pi * arccos(-||x - y|| / sigma) - 2 / pi * ||x - y|| / sigma * sqrt(1 - (||x - y|| / sigma)^2)
// positive-definite in R^2
public class CircularKernel implements KernelFunction {

    // parameter of circular kernel
    private double sigma;

    private EuclideanDistance euclidean;

    // use default value 1.0 for sigma
    public CircularKernel() {
        sigma = 1.0;
        euclidean = new EuclideanDistance();
    }

    public CircularKernel(double sigma) {
        this.sigma = sigma;
        euclidean = new EuclideanDistance();
    }

    @Override
    public double kernel(Data x, Data y) {
        double euclid = euclidean.distance(x, y);
        if (euclid >= sigma) {
            return 0.0;
        }
        euclid = euclid / sigma;
        return 2.0 / Math.PI * (Math.acos(-euclid) - euclid * Math.sqrt(1.0 - Math.pow(euclid, 2)));
    }

    @Override
    public double kernel(Data x) {
        return 1.0;
    }

    @Override
    public boolean constantTrace() {
        return true;
    }
}
