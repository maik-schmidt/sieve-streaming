package submodular.kernel;

import main.Data;
import submodular.distance.EuclideanDistance;

// k(x, y) = sigma / ||x - y|| * sin(||x - y|| / sigma)
// positive-definite in R^3
public class WaveKernel implements KernelFunction {

    // parameter of wave kernel
    private double sigma;

    private EuclideanDistance euclidean;

    // use default value 1.0 for sigma
    public WaveKernel() {
        this.sigma = 1.0;
        euclidean = new EuclideanDistance();
    }

    public WaveKernel(double sigma) {
        this.sigma = sigma;
        euclidean = new EuclideanDistance();
    }

    @Override
    public double kernel(Data x, Data y) {
        double euclid = euclidean.distance(x, y);
        //System.out.println(euclid);
        return sigma / euclid * Math.sin(euclid / sigma);
    }

    @Override
    public double kernel(Data x) {
        return 1.0;
    }

    @Override
    public boolean constantTrace() {
        return true;
    }
}
