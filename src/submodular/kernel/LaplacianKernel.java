package submodular.kernel;

import main.Data;
import submodular.distance.ManhattanDistance;

// k(x, y) = e^(-|x - y| / (2 * sigma^2)) = e^(-gamma * |x - y|)
// positive-definite
public class LaplacianKernel implements KernelFunction {

    // parameter of abel kernel
    private double gamma;

    private ManhattanDistance manhatten;

    // use default value 1.0 for sigma
    public LaplacianKernel() {
        gamma = 1.0 / 2.0;
        manhatten = new ManhattanDistance();
    }

    public LaplacianKernel(double sigma) {
        gamma = 1.0 / (2.0 * sigma * sigma);
        manhatten = new ManhattanDistance();
    }

    @Override
    public double kernel(Data x, Data y) {
        double manhattan = manhatten.distance(x, y);
        return Math.exp(-gamma * manhattan);
    }

    @Override
    public double kernel(Data x) {
        return 1.0;
    }

    @Override
    public boolean constantTrace() {
        return true;
    }
}
