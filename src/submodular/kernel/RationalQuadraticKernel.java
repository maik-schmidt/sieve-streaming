package submodular.kernel;

import main.Data;
import submodular.distance.SquaredEuclideanDistance;

// k(x, y) = 1 - ||x - y||^2 / (||x - y||^2 + c)
// positive-definite
public class RationalQuadraticKernel implements KernelFunction {

    // parameter of rational quadratic kernel
    private double c;

    private SquaredEuclideanDistance squaredEuclidean;

    public RationalQuadraticKernel(double c) {
        this.c = c;
        squaredEuclidean = new SquaredEuclideanDistance();
    }

    @Override
    public double kernel(Data x, Data y) {
        double squaredEuclid = squaredEuclidean.distance(x, y);
        return 1.0 - squaredEuclid / (squaredEuclid + c);
    }

    @Override
    public double kernel(Data x) {
        return 1.0;
    }

    @Override
    public boolean constantTrace() {
        return true;
    }
}
