package submodular.kernel;

import main.Data;

public interface KernelFunction {

    // kernel indicates similarity between two vectors
    double kernel(Data x, Data y);

    double kernel(Data x);

    // trace of kernel matrix is used for lower bound of determinant: det(I + A) <= log(1 + trace(A))
    boolean constantTrace();
}
