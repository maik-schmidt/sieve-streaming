package submodular.kernel;

import main.Data;
import submodular.distance.DotProduct;

// k(x, y) = x^T * y + c
// positive-definite
public class LinearKernel implements KernelFunction {

    // parameter of linear kernel
    private double c;

    private DotProduct dot;

    // use default value 0.0 for c
    public LinearKernel() {
        c = 0.0;
        dot = new DotProduct();
    }

    public LinearKernel(double c) {
        this.c = c;
        dot = new DotProduct();
    }

    @Override
    public double kernel(Data x, Data y) {
        return dot.distance(x, y) + c;
    }

    @Override
    public double kernel(Data x) {
        return kernel(x, x);
    }

    @Override
    public boolean constantTrace() {
        return false;
    }
}
