package submodular.kernel;

import main.Data;
import submodular.distance.DotProduct;

// k(x, y) = (a * x^T * y + c)^d
// positive-definite
public class PolynomialKernel implements KernelFunction {

    // parameters of polynomial kernel
    private double a;
    private double c;
    private double d;

    private DotProduct dot;

    // use default values 1.0 for a and d, 0.0 for c
    public PolynomialKernel() {
        a = 1.0;
        c = 0.0;
        d = 1.0;
        dot = new DotProduct();
    }

    public PolynomialKernel(double a, double c, double d) {
        this.a = a;
        this.c = c;
        this.d = d;
        dot = new DotProduct();
    }

    @Override
    public double kernel(Data x, Data y) {
        return Math.pow(a * dot.distance(x, y) + c, d);
    }

    @Override
    public double kernel(Data x) {
        return kernel(x, x);
    }

    @Override
    public boolean constantTrace() {
        return false;
    }
}
