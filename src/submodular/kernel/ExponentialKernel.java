package submodular.kernel;

import main.Data;
import submodular.distance.EuclideanDistance;

// k(x, y) = e^(-||x - y|| / (2 * sigma^2)) = e^(-gamma * ||x - y||)
// positive-definite
public class ExponentialKernel implements KernelFunction {

    // parameter of exponential kernel
    private double gamma;

    private EuclideanDistance euclidean;

    // use default value 1.0 for sigma
    public ExponentialKernel() {
        gamma = 1.0 / 2.0;
        euclidean = new EuclideanDistance();
    }

    public ExponentialKernel(double sigma) {
        gamma = 1.0 / (2.0 * sigma * sigma);
        euclidean = new EuclideanDistance();
    }

    @Override
    public double kernel(Data x, Data y) {
        double euclid = euclidean.distance(x, y);
        return Math.exp(-gamma * euclid);
    }

    @Override
    public double kernel(Data x) {
        return 1.0;
    }

    @Override
    public boolean constantTrace() {
        return true;
    }
}
