package submodular;

import main.Data;
import main.Sieve;
import submodular.kernel.KernelFunction;
import util.Matrix;

// f(S) = 1/2 * log det(Id + sigma^-2 * kernelMatrix(S,S)) = sum_i log cholesky(S)_ii
public class InformativeVectorMachine implements SubmodularFunction {

    public static int oracleQueries;

    public static double logBase = Math.log(2);

    // regularization parameter of objective function
    public double _sigma;

    private KernelFunction kernelFunction;

    // use default value 1.0 for sigma
    public InformativeVectorMachine(KernelFunction kernelFunction) {
        _sigma = 1.0;
        this.kernelFunction = kernelFunction;
    }

    public InformativeVectorMachine(double sigma, KernelFunction kernelFunction) {
        _sigma = 1.0 / (sigma * sigma);
        this.kernelFunction = kernelFunction;
    }

    // calculate objective function of a single element
    @Override
    public double utility(Data element) {
        return Math.log(1.0 + kernelFunction.kernel(element) * _sigma) / 2 / logBase;
        //return Math.log(kernelFunction.kernel(element) == 0.0 ? 1.0 : _sigma + 1.0) / 2;
    }

    // calculate objective function of a sieve's summary
    @Override
    public double utility(Sieve sieve) {
        return sieve.getUtility();
    }

    // calculate objective function of the union of a sieve's summary and an element
    //@Override
    public double utility(Sieve sieve, Data element) {
        oracleQueries += 1;

        Data[] summary = sieve.getSummary();
        int size = sieve.getSize();

        double[][] cholesky = sieve.getCholesky();
        for (int column = 0; column < size; column++) {

            // kernel function
            cholesky[size][column] = kernelFunction.kernel(summary[column], element) * _sigma;

            //normalize
            //double kernel = kernelFunction.kernel(summary[column], element);
            //double kernelX = kernelFunction.kernel(summary[column], summary[column]);
            //double kernelY = kernelFunction.kernel(element, element);
            //double normalizedKernel = kernelX == 0.0 || kernelY == 0.0 ? 0.0 : kernel / Math.sqrt(kernelX * kernelY);
            //cholesky[size][column] = normalizedKernel * _sigma;
        }
        cholesky[size][size] = kernelFunction.kernel(element) * _sigma + 1.0;
        //cholesky[size][size] = kernelFunction.kernel(element) == 0.0 ? 1.0 : _sigma + 1.0;

        // decompose added row
        Matrix.choleskyUpdate(cholesky, size + 1);

        // calculate log determinant with main diagonal of cholesky decomposition
        return sieve.getUtility() + Math.log(cholesky[size][size]) / logBase;
    }

    public double get_sigma() {
        return _sigma;
    }

    public KernelFunction getKernelFunction() {
        return kernelFunction;
    }


}