package util;

public class Matrix {

    // decomposition of positive-definite matrix into the product of a lower triangular matrix and its conjugate transpose
    // for fast and save computation of the log determinant
    public static void choleskyDecomposition(double[][] cholesky, int size) {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < i; j++) {
                double sum = cholesky[i][j];
                for (int k = 0; k < j; k++) {
                    sum -= cholesky[i][k] * cholesky[j][k];
                }
                cholesky[i][j] = sum / cholesky[j][j];
            }
            double sum = cholesky[i][i];
            for (int k = 0; k < i; k++) {
                sum -= cholesky[i][k] * cholesky[i][k];
            }
            if (sum > 0) {
                cholesky[i][i] = Math.sqrt(sum);
            } else {
                throw new RuntimeException("Matrix is not positive-definite.");
            }
        }
        //printMatrix(cholesky, size);
    }

    // decomposition of positive-definite matrix into the product of a lower triangular matrix and its conjugate transpose
    // for fast and save computation of the log determinant
    // needs to be called after every element addition to update cholesky matrix
    public static void choleskyUpdate(double[][] cholesky, int size) {
        int i = size - 1;
        for (int j = 0; j < i; j++) {
            double sum = cholesky[i][j];
            for (int k = 0; k < j; k++) {
                sum -= cholesky[i][k] * cholesky[j][k];
            }
            cholesky[i][j] = sum / cholesky[j][j];
        }
        double sum = cholesky[i][i];
        for (int k = 0; k < i; k++) {
            sum -= cholesky[i][k] * cholesky[i][k];
        }
        if (sum > 0) {
            cholesky[i][i] = Math.sqrt(sum);
        } else {
            throw new RuntimeException("Matrix is not positive-definite.");
        }
        //printMatrix(cholesky, size);
    }

    public static void printMatrix(double[][] matrix, int size) {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                System.out.print(matrix[i][j] + ", ");
            }
            System.out.println();
        }
        System.out.println();
    }
}
