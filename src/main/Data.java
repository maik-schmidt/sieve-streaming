package main;

public class Data {

    // unique identifier
    private int id;

    // values of this data point
    private double[] values;

    // optional variable for labeled data
    private int label;

    public Data(int id, double value) {
        this.id = id;
        this.values = new double[1];
        values[0] = value;
    }

    public Data(int id, double[] values) {
        this.id = id;
        this.values = values;
    }

    public Data(int id, int label, double[] values) {
        this.id = id;
        this.label = label;
        this.values = values;
    }

    public int getID() {
        return id;
    }

    public double[] getValues() {
        return values;
    }

    public double getValue(int i) {
        return values[i];
    }

    public int getLabel() {
        return label;
    }

    public void setLabel(int label) {
        this.label = label;
    }
}
