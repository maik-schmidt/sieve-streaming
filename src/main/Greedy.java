package main;

import submodular.KMedoid;
import submodular.SubmodularFunction;

import java.util.Arrays;

/**
 * Implementation of greedy submodular function maximization. Starts with an empty set, in every iteration the
 * element with the highest marginal value is added.
 */

public class Greedy {

    private Sieve sieve;
    private SubmodularFunction submodularFunction;

    public Greedy(int k, SubmodularFunction submodularFunction) {
        this.submodularFunction = submodularFunction;
        sieve = new Sieve(0.0, k, submodularFunction);
    }

    // greedy algorithm, adds element with highest marginal value to summary
    public void process(Data[] elements) {
        int argMax = findMax(elements);
        sieve.setUtility(submodularFunction.utility(sieve, elements[argMax]));
        sieve.add(elements[argMax]);

        if (submodularFunction instanceof KMedoid) {
            sieve.updateDistance(((KMedoid) submodularFunction).getDistance());
        }
    }

    // find element with maximum marginal gain from start (inclusive) to end (exclusive)
    private int findMax(Data[] elements) {
        double max = 0.0;
        int argMax = 0;
        for (int i = 0; i < elements.length; i++) {
            if (!sieve.contains(elements[i])) {
                double unionUtility = submodularFunction.utility(sieve, elements[i]);
                double utility = submodularFunction.utility(sieve);
                double marginalGain = unionUtility - utility;
                if (marginalGain > max) {
                    max = marginalGain;
                    argMax = i;
                }
            }
        }
        return argMax;
    }

    public Data[] getSummary() {
        System.out.println("Greedy Utility: " + sieve.getUtility());

        return Arrays.copyOfRange(sieve.getSummary(), 0, sieve.getSize());
    }
}
