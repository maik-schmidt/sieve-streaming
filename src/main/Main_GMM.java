package main;

import submodular.InformativeVectorMachine;
import submodular.SubmodularFunction;
import submodular.kernel.GaussianKernel;
import submodular.kernel.KernelFunction;
import util.CsvReader;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Main_GMM {

    // file name of input data in csv format
    private final static String FILE_NAME = "res/gmm/out.csv";

    // size of summaries
    private final static int k = 100;
    private final static double epsilon = 0.1;
    private final static int reservoirSize = 10000;

    // parameter of kernels
    private final static double gaussianSigma = 25;
    private final static double exponentialSigma = 3.0;
    private final static double rationalQuadraticC = 10;
    private final static double laplacianSigma = 2.0;

    private static SieveStreaming sieveStreaming;
    private static Greedy greedy;
    private static ReservoirSampling reservoirSampling;
    private static KernelFunction kernel = new GaussianKernel(gaussianSigma);
    private static InformativeVectorMachine ivm = new InformativeVectorMachine(kernel);

    public static void main(String[] args) {

        reservoirSampling();
        Data[] reservoir = reservoirSampling.getReservior();
        printSummary(reservoir);

        //Sieve Streaming
        long time = System.currentTimeMillis();
        calculateSieveStreaming(ivm);
        System.out.println("Sieve Streaming execution time (seconds): " + (System.currentTimeMillis() - time) / 1000.0);
        printSummary(sieveStreaming.getSummary());

        //Greedy
        time = System.currentTimeMillis();
        calculateGreedy(ivm);
        System.out.println("Greedy execution time (seconds): " + (System.currentTimeMillis() - time) / 1000.0);
        printSummary(greedy.getSummary());
    }

    // calculate a summary of objects using the sieve streaming algorithm
    private static void calculateSieveStreaming(SubmodularFunction submodularFunction) {
        sieveStreaming = new SieveStreaming(k, epsilon, submodularFunction);
        Data data;

        try {
            InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
            CsvReader reader = new CsvReader(new InputStreamReader(inputStream));

            // skip header
            reader.readRecord();

            // read line
            for (int id = 0; reader.readRecord(); id++) {
                String[] line = reader.getValues();
                double[] values = new double[line.length];
                for (int i = 0; i < line.length; i++) {
                    values[i] = Double.parseDouble(line[i]);
                }
                data = new Data(id, values);

                // test marginal value against threshold of all sieves
                sieveStreaming.process(data);
            }
            sieveStreaming.shutdownThreads();
            reader.close();
        } catch (FileNotFoundException e) {
            System.err.println("File " + FILE_NAME + " not found.");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // calculate a summary of objects using the greedy algorithm
    private static void calculateGreedy(SubmodularFunction submodularFunction) {
        greedy = new Greedy(k, submodularFunction);
        Data[] data;

        try {

            // count rows
            InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
            CsvReader reader = new CsvReader(new InputStreamReader(inputStream));

            // skip header
            reader.readRecord();

            int ids = 0;
            while (reader.readRecord()) {
                ids++;
            }
            data = new Data[ids];
            reader.close();

            inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
            reader = new CsvReader(new InputStreamReader(inputStream));

            // skip header
            reader.readRecord();

            // read data
            for (int id = 0; reader.readRecord(); id++) {
                String[] line = reader.getValues();
                double[] values = new double[line.length];
                for (int i = 0; i < line.length; i++) {
                    values[i] = Double.parseDouble(line[i]);
                }
                data[id] = new Data(id, values);
            }
            reader.close();

            // add data with highest marginal value to summary
            for (int i = 0; i < k; i++) {
                greedy.process(data);
            }
        } catch (FileNotFoundException e) {
            System.err.println("File " + FILE_NAME + " not found.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void reservoirSampling() {
        reservoirSampling = new ReservoirSampling(reservoirSize, 1);
        Data data;

        try {
            InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
            CsvReader reader = new CsvReader(new InputStreamReader(inputStream));

            // skip header
            reader.readRecord();

            // read line
            for (int id = 0; reader.readRecord(); id++) {
                String[] line = reader.getValues();
                double[] values = new double[line.length];
                for (int i = 0; i < line.length; i++) {
                    values[i] = Double.parseDouble(line[i]);
                }
                data = new Data(id, values);

                reservoirSampling.sample(data, id);
            }
            reader.close();
        } catch (FileNotFoundException e) {
            System.err.println("File " + FILE_NAME + " not found.");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void printSummary(Data[] summary) {
        for (int i = 0; i < summary.length; i++) {
            System.out.println(summary[i].getValue(0) + "," + summary[i].getValue(1));
        }
        System.out.println("Last ID: " + summary[summary.length - 1].getID());
    }
}
