package main;

import submodular.InformativeVectorMachine;
import submodular.KMedoid;
import submodular.SubmodularFunction;

public class Sieve {

    // estimator for optimal value of summary, is used to calculate the threshold
    private double value;

    // subset of stream with marginal gain greater than threshold
    private Data[] summary;

    // size of summary
    private int size;

    // utility of summary
    private double utility;

    // threshold for marginal gains
    private double threshold;

    // cholesky decomposition of kernel matrix, represents similarity of elements in the summary
    private double[][] cholesky;

    // distance matrix for pairwise elements of summary and
    private double[] distance;

    public Sieve(double value, int k, SubmodularFunction submodularFunction) {
        summary = new Data[k];
        this.value = value;

        if (submodularFunction instanceof InformativeVectorMachine) {
            cholesky = new double[k][k];
        } else if (submodularFunction instanceof KMedoid) {
            distance = new double[((KMedoid) submodularFunction).getReservoir().length];
            for (int i = 0; i < distance.length; i++) {
                distance[i] = Double.POSITIVE_INFINITY;
            }
        }
    }

    public void add(Data element) {
        summary[size] = element;
        size++;
    }

    public boolean contains(Data element) {
        for (int i = 0; i < size; i++) {
            if (summary[i].getID() == element.getID()) {
                return true;
            }
        }
        return false;
    }

    public void updateDistance(double[] distance) {
        for (int i = 0; i < distance.length; i++) {
            if (distance[i] < this.distance[i]) {
                this.distance[i] = distance[i];
            }
        }
    }

    public int getSize() {
        return size;
    }

    public double getValue() {
        return value;
    }

    public Data[] getSummary() {
        return summary;
    }

    public double getUtility() {
        return utility;
    }

    public void setUtility(double utility) {
        this.utility = utility;
    }

    public double getThreshold() {
        return threshold;
    }

    public void setThreshold(double threshold) {
        this.threshold = threshold;
    }

    public double[][] getCholesky() {
        return cholesky;
    }

    public double[] getDistance() {
        return distance;
    }
}
