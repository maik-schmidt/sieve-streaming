package main;

import submodular.InformativeVectorMachine;
import submodular.KMedoid;
import submodular.SubmodularFunction;
import submodular.kernel.KernelFunction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Select a subset of k data points from the stream that are most representative according to the objective function utility,
 * using the sieve-streaming algorithm. Badanidiyuru, Ashwinkumar, et al. "Streaming submodular maximization:
 * Massive data summarization on the fly." ACM, 2014.
 */

public class SieveStreaming {

    private static boolean debug = false;

    // 1: [log(1 + k), km], 2: [log(1 + k), 2km], 3: [m, 2km]
    private static int sieveInterval = 1;

    // size of thread pool
    private int threads;

    // thread pool
    private ExecutorService executorService;

    // number of maximum elements in a summary
    private int k;

    // parameter to control approximation guarantee
    private double epsilon;

    // monotone submodular function to maximize
    private SubmodularFunction submodularFunction;

    // list of currently maintained sieves
    private List<Sieve> sieves;

    // maximum utility of a single element
    private double m;
    private double value = 1.0;

    public SieveStreaming(int k, double epsilon, SubmodularFunction submodularFunction) {
        sieves = new ArrayList<>();
        this.k = k;
        this.epsilon = epsilon;
        this.submodularFunction = submodularFunction;

        if (submodularFunction instanceof InformativeVectorMachine && sieveInterval < 3) {
            KernelFunction kernelFunction = ((InformativeVectorMachine) submodularFunction).getKernelFunction();
            if (kernelFunction.constantTrace()) {

                // limit sieve interval to [2 * min f(S),  (2 *) max f(S)] for |S| = k
                double constant = kernelFunction.kernel(new Data(-1, 1));
                double _sigma = ((InformativeVectorMachine) submodularFunction).get_sigma();
                m = Math.log(1 + _sigma * constant) / 2 / InformativeVectorMachine.logBase;
                generateSieves(Math.log(1 + k * _sigma * constant) / InformativeVectorMachine.logBase, sieveInterval == 1 ? k * m : 2 * k * m);
            }
        }
    }

    public SieveStreaming(int k, double epsilon, SubmodularFunction submodularFunction, int threads) {
        sieves = new ArrayList<>();
        this.k = k;
        this.epsilon = epsilon;
        this.submodularFunction = submodularFunction;

        if (submodularFunction instanceof InformativeVectorMachine && sieveInterval < 3) {
            KernelFunction kernelFunction = ((InformativeVectorMachine) submodularFunction).getKernelFunction();
            if (kernelFunction.constantTrace()) {

                // limit sieve interval to [2 * min f(S),  (2 *) max f(S)] for |S| = k
                double constant = kernelFunction.kernel(new Data(-1, 1));
                double _sigma = ((InformativeVectorMachine) submodularFunction).get_sigma();
                m = Math.log(1 + _sigma * constant) / 2 / InformativeVectorMachine.logBase;
                generateSieves(Math.log(1 + k * _sigma * constant) / InformativeVectorMachine.logBase, sieveInterval == 1 ? k * m : 2 * k * m);
            }
        }

        this.threads = threads;
        if (threads > 1) {
            executorService = Executors.newFixedThreadPool(threads);
        }
    }

    // sieve-streaming algorithm
    public void process(final Data element) {
        updateSieves(element);

        if (threads <= 1) {
            for (Sieve sieve : sieves) {
                sieve(sieve, element);
            }

        } else {
            List<Callable<Void>> tasks = new ArrayList<>();
            for (Sieve sieve : sieves) {
                final Sieve s = sieve;
                Callable<Void> task = new Callable<Void>() {
                    @Override
                    public Void call() throws Exception {
                        sieve(s, element);
                        return null;
                    }
                };
                tasks.add(task);
            }
            try {
                executorService.invokeAll(tasks);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void sieve(Sieve sieve, Data element) {

        // check if sieve is full
        if (sieve.getSize() < k) {
            double unionUtility = submodularFunction.utility(sieve, element);
            double utility = submodularFunction.utility(sieve);
            double marginalGain = unionUtility - utility;

            // add element to summary
            if (marginalGain >= sieve.getThreshold()) {
                sieve.add(element);
                sieve.setUtility(unionUtility);
                sieve.setThreshold((sieve.getValue() / 2 - unionUtility) / (k - sieve.getSize()));

                if (submodularFunction instanceof KMedoid) {
                    sieve.updateDistance(((KMedoid) submodularFunction).getDistance());
                }
            }
        }
    }

    private void generateSieves(double minimum, double maximum) {
        if (maximum <= minimum) {
            maximum = minimum * (1 + epsilon);
        }
        if (minimum < 1) {
            while (value > minimum) {
                value /= (1 + epsilon);
            }
            value *= (1 + epsilon);
        } else {
            while (value < minimum) {
                value *= (1 + epsilon);
            }
        }
        while (value <= maximum) {
            Sieve sieve = new Sieve(value, k, submodularFunction);
            double threshold = value / (2 * k);
            sieve.setThreshold(threshold);
            sieves.add(sieve);
            if (debug) {
                System.out.println("Sieve " + (sieves.size() - 1) + " added with Value " + value + " and Threshold " + threshold);
            }
            value *= (1 + epsilon);
        }
    }

    // maintain sieves with values in interval [m, 2 * k * m]
    private void updateSieves(Data element) {
        double m = submodularFunction.utility(element);
        if (m > this.m) {
            this.m = m;

            // delete sieves outside of interval
            while (!sieves.isEmpty() && sieves.get(0).getValue() < m) {
                System.out.println("Sieve with Value " + sieves.get(0).getValue() + " removed");
                sieves.remove(0);
            }

            // add new sieves
            generateSieves(m, 2 * k * m);
        }
    }

    // find best summary
    public Data[] getSummary() {
        double bestUtility = 0;
        int bestSieve = 0;
        for (int i = 0; i < sieves.size(); i++) {
            double utility = sieves.get(i).getUtility();
            if (debug) {
                System.out.println("Utility of sieve " + i + ": " + utility + " (" + sieves.get(i).getSize() + " elements)");
            }
            if (utility > bestUtility) {
                bestUtility = utility;
                bestSieve = i;
            }
        }
        System.out.println("Best sieve: " + bestSieve + ", Utility: " + bestUtility);

        return Arrays.copyOfRange(sieves.get(bestSieve).getSummary(), 0, sieves.get(bestSieve).getSize());
    }

    // find best summary with reservoir
    public Data[] getSummary(Data[] reservoir) {

        // non-full sets are augmented by greedily adding elements from reservoir
        for (Sieve sieve : sieves) {
            for (int i = 0; sieve.getSize() < k; i++) {
                if (!sieve.contains(reservoir[i])) {
                    double unionUtility = submodularFunction.utility(sieve, reservoir[i]);
                    sieve.add(reservoir[i]);
                    sieve.setUtility(unionUtility);
                }
            }
        }

        return getSummary();
    }

    public void shutdownThreads() {
        if (executorService != null) {
            executorService.shutdown();
        }
    }
}