package main;

import submodular.InformativeVectorMachine;
import submodular.SubmodularFunction;
import submodular.kernel.KernelFunction;
import util.Matrix;

import java.util.Random;

/**
 * Implementation of Random Sampling with a Reservoir.
 */

public class ReservoirSampling {

    private Data[] reservoir;
    private Random random;

    public ReservoirSampling(int k) {
        reservoir = new Data[k];
        random = new Random();
    }

    public ReservoirSampling(int k, int seed) {
        reservoir = new Data[k];
        random = new Random(seed);
    }

    public void sample(Data element, int i) {

        // fill reservoir
        if (i < reservoir.length) {
            reservoir[i] = element;

            // replace elements with gradually decreasing probability
        } else {
            int j = random.nextInt(i) + 1;
            if (j < reservoir.length) {
                reservoir[j] = element;
            }
        }
    }

    public double getUtility(SubmodularFunction submodularFunction) {
        if (submodularFunction instanceof InformativeVectorMachine) {
            KernelFunction kernelFunction = ((InformativeVectorMachine) submodularFunction).getKernelFunction();
            double _sigma = ((InformativeVectorMachine) submodularFunction).get_sigma();
            int k = reservoir.length;
            double[][] cholesky = new double[k][k];
            for (int i = 0; i < k; i++) {
                for (int j = 0; j < i; j++) {
                    cholesky[i][j] = kernelFunction.kernel(reservoir[i], reservoir[j]) * _sigma;
                }
                cholesky[i][i] = kernelFunction.kernel(reservoir[i]) * _sigma + 1.0;
            }
            Matrix.choleskyDecomposition(cholesky, k);
            double utility = 0.0;
            for (int i = 0; i < k; i++) {
                utility += Math.log(cholesky[i][i]) / InformativeVectorMachine.logBase;
            }
            return utility;
        }
        return 0.0;
    }

    public Data[] getReservior() {
        return reservoir;
    }
}
