package main;

import submodular.InformativeVectorMachine;
import submodular.SubmodularFunction;
import submodular.kernel.GaussianKernel;
import submodular.kernel.KernelFunction;
import util.CsvReader;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Main_Parkinsons {

    // file name of input data in csv format
    private final static String FILE_NAME = "res/parkinsons/parkinsons_normalized.csv";

    // size of summaries
    private final static int k = 20;
    private final static double epsilon = 0.1;

    // parameter of kernels
    private final static double gkSigma = 1;

    private static SieveStreaming sieveStreaming;
    private static Greedy greedy;
    private static ReservoirSampling reservoirSampling;
    private static KernelFunction kernel = new GaussianKernel(gkSigma);
    private static InformativeVectorMachine ivm = new InformativeVectorMachine(kernel);

    public static void main(String[] args) {

        // Reservoir Sampling
        long time = System.currentTimeMillis();
        reservoirSampling();
        System.out.println("Reservoir sampling execution time (seconds): " + (System.currentTimeMillis() - time) / 1000.0);
        System.out.println(reservoirSampling.getUtility(ivm));
        System.out.println("Oracle Queries: 1");

        // Sieve Streaming
        time = System.currentTimeMillis();
        calculateSieveStreaming(ivm);
        System.out.println("Sieve Streaming execution time (seconds): " + (System.currentTimeMillis() - time) / 1000.0);
        sieveStreaming.getSummary();
        System.out.println("Oracle Queries: " + InformativeVectorMachine.oracleQueries);
        InformativeVectorMachine.oracleQueries = 0;

        // Greedy
        time = System.currentTimeMillis();
        calculateGreedy(ivm);
        System.out.println("Greedy execution time (seconds): " + (System.currentTimeMillis() - time) / 1000.0);
        greedy.getSummary();
        System.out.println("Oracle Queries: " + InformativeVectorMachine.oracleQueries);
        InformativeVectorMachine.oracleQueries = 0;

    }

    // calculate a summary of objects using the sieve streaming algorithm
    private static void calculateSieveStreaming(SubmodularFunction submodularFunction) {
        sieveStreaming = new SieveStreaming(k, epsilon, submodularFunction);
        Data data;

        try {

            // read line
            InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
            CsvReader reader = new CsvReader(new InputStreamReader(inputStream));
            for (int id = 0; reader.readRecord(); id++) {
                String[] line = reader.getValues();
                double[] values = new double[line.length];
                for (int i = 0; i < line.length; i++) {
                    values[i] = Double.parseDouble(line[i]);
                }
                data = new Data(id, values);

                // test marginal value against threshold of all sieves
                sieveStreaming.process(data);
            }
            reader.close();
        } catch (FileNotFoundException e) {
            System.err.println("File " + FILE_NAME + " not found.");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // calculate a summary of objects using the greedy algorithm
    private static void calculateGreedy(SubmodularFunction submodularFunction) {
        greedy = new Greedy(k, submodularFunction);
        Data[] data;

        try {

            // count rows
            InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
            CsvReader reader = new CsvReader(new InputStreamReader(inputStream));
            int ids = 0;
            while (reader.readRecord()) {
                ids++;
            }
            data = new Data[ids];
            reader.close();

            // read data
            inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
            reader = new CsvReader(new InputStreamReader(inputStream));
            for (int id = 0; reader.readRecord(); id++) {
                String[] line = reader.getValues();
                double[] values = new double[line.length];
                for (int i = 0; i < line.length; i++) {
                    values[i] = Double.parseDouble(line[i]);
                }
                data[id] = new Data(id, values);
            }
            reader.close();

            // add data with highest marginal value to summary
            for (int i = 0; i < k; i++) {
                greedy.process(data);
            }
        } catch (FileNotFoundException e) {
            System.err.println("File " + FILE_NAME + " not found.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void reservoirSampling() {
        reservoirSampling = new ReservoirSampling(k, 1);
        Data data;

        try {

            // read line
            InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
            CsvReader reader = new CsvReader(new InputStreamReader(inputStream));
            for (int id = 0; reader.readRecord(); id++) {
                String[] line = reader.getValues();
                double[] values = new double[line.length];
                for (int i = 0; i < line.length; i++) {
                    values[i] = Double.parseDouble(line[i]);
                }
                data = new Data(id, values);

                reservoirSampling.sample(data, id);
            }
            reader.close();
        } catch (FileNotFoundException e) {
            System.err.println("File " + FILE_NAME + " not found.");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
